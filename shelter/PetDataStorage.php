<?php

namespace shelter;

/**
 * Class PetDataStorage
 * @package shelter
 */
interface PetDataStorage
{
    /**
     * @param string $sort
     * @return Pet[]
     */
    function getAll($sort = '');

    /**
     * @param $type
     * @param string $sort
     * @return Pet[]
     */
    function getAllByType($type, $sort = '');

    /**
     * @param Pet $pet
     * @return mixed
     */
    function push(Pet $pet);

    /**
     * @param string $type
     * @return mixed
     */
    function pull(string $type = '');
}