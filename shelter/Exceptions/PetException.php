<?php

namespace shelter\Exceptions;

/**
 * Class PetException
 * @package shelter\Exceptions
 */
class PetException extends \Exception
{
    public function printMessage()
    {
        echo $this->getMessage() . '<br />';
        echo '<pre>';
        print_r($this->getTrace());
        echo '</pre>';
    }
}