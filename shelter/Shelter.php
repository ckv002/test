<?php

namespace shelter;

/**
 * Class Shelter
 * @package shelter
 */
class Shelter
{
    /**
     * @var PetDataStorage
     */
    protected $storage;

    /**
     * Shelter constructor.
     * @param PetDataStorage $storage
     */
    public function __construct(PetDataStorage $storage)
    {
        $this->setStorage($storage);
    }

    /**
     * @return PetDataStorage
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param PetDataStorage $storage
     */
    protected function setStorage(PetDataStorage $storage)
    {
        $this->storage = $storage;
    }
}