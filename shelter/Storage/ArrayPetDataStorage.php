<?php

namespace shelter\Storage;

use shelter\Pet;
use shelter\PetDataStorage;
use shelter\Exceptions\PetTypeException;

/**
 * Class ArrayPetDataStorage
 * @package shelter\Storage
 */
class ArrayPetDataStorage implements PetDataStorage
{
    protected $pets = [];

    /**
     * @param string $sort
     * @return array
     */
    public function getAll($sort = '')
    {
        return ($sort) ? $this->sort($this->pets, $sort) : $this->pets;
    }

    /**
     * @param Pet $pet
     * @return void
     */
    public function push(Pet $pet)
    {
        $this->pets[] = $pet;
    }

    /**
     * @param $type
     * @param string $sort
     * @return array|mixed
     */
    public function getAllByType($type, $sort = '')
    {
        $data = [];

        foreach ($this->pets as $pet) {
            if (get_class($pet) == $type) {
                $data[] = $pet;
            }
        }

        return ($sort) ? $this->sort($data, 'name') : $data;
    }

    /**
     * @param string $type
     * @return mixed
     * @throws PetTypeException
     */
    public function pull(string $type = '')
    {
        if ($type && !class_exists($type)) {
            throw new PetTypeException('Unknown animal type.');
        }

        // Тут по-хорошему сложнее бы сделать, но тестового задания, думаю, пойдёт.
        $data = ($type) ? $this->getAllByType($type, 'staySeconds') : $this->getAll('staySeconds');

        $pet = clone(end($data));
        $this->remove($pet);

        return $pet;
    }

    /**
     * @param $pets
     * @param $field
     * @return mixed
     */
    protected function sort($pets, $field)
    {
        usort($pets, function (Pet $a, Pet $b) use ($field) {
            if ($a->get($field) == $b->get($field)) {
                return 0;
            }

            return ($a->get($field) < $b->get($field)) ? -1 : 1;
        });

        return $pets;
    }

    /**
     * @param Pet $pet
     * @return bool
     */
    protected function remove(Pet $pet)
    {
        $success = false;

        foreach ($this->pets as $key => $value) {
            if ($pet == $value) {
                unset($this->pets[$key]);
                $success = true;
            }
        }

        return $success;
    }
}