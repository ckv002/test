<?php

namespace shelter;
use shelter\Exceptions\PetFieldException;
use shelter\Exceptions\PetAgeException;

/**
 * Class Pet
 * @package shelter
 */
class Pet
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $birth;

    /**
     * @var \DateTime
     */
    protected $taken;

    /**
     * Pet constructor.
     * @param string $name
     * @param \DateTime $birth
     * @throws PetAgeException
     */
    public function __construct(string $name, \DateTime $birth)
    {
        if ((time() - $birth->getTimestamp()) <= 0) {
            throw new PetAgeException('Too young.');
        }

        $this->setName($name);
        $this->setBirth($birth);
        $this->setTaken(new \DateTime());
    }

    /**
     * @param $name
     */
    protected function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \DateTime $birth
     */
    protected function setBirth(\DateTime $birth)
    {
        $this->birth = $birth;
    }

    /**
     * @return \DateTime
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * @param \DateTime $time
     */
    protected function setTaken($time)
    {
        $this->taken = $time;
    }

    /**
     * @return \DateTime
     */
    public function getTaken()
    {
        return $this->taken;
    }

    /**
     * @return \DateInterval
     */
    public function getAge()
    {
        return $this->getBirth()->diff(new \DateTime());
    }

    /**
     * @return \DateInterval
     */
    public function getStay()
    {
        return $this->getTaken()->diff(new \DateTime());
    }

    /**
     * @return int
     */
    public function getStaySeconds()
    {
        return $this->getTaken()->diff(new \DateTime())->s;
    }

    /**
     * @param $field
     * @return mixed
     * @throws PetFieldException
     */
    public function get($field)
    {
        $method = 'get' . ucfirst($field);

        if (method_exists($this, $method)) {
            return $this->{$method}();
        } else {
            throw new PetFieldException("Unknown method {$method}()");
        }
    }
}