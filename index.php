<?php

spl_autoload_register(function ($class) {
    require_once __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';
});

use \shelter\Shelter;
use \shelter\Pet\Dog;
use \shelter\Pet\Cat;
use \shelter\Pet\Turtle;
use \shelter\Storage\ArrayPetDataStorage;
use \shelter\Exceptions\PetAgeException;
use \shelter\Exceptions\PetTypeException;

$shelter = new Shelter(new ArrayPetDataStorage());

// Создаём животных, ловим исключение (не пытаются ли нам сдать животное из будущего?)
// Спим секунду после каждого добавления чтобы наглядно видеть разницу во времени содержания
try {

    // Немного собак
    $pets[] = new Dog('Тишка', new DateTime('2018-01-01'));
    sleep(1);
    $pets[] = new Dog('Палкан', new DateTime('2018-01-10'));
    sleep(1);
    $pets[] = new Dog('Виталий Валентинович', new DateTime('1974-01-23'));

    // Немного кошек
    $pets[] = new Cat('Том', new DateTime('2018-02-09'));
    sleep(1);
    $pets[] = new Cat('Пушок', new DateTime('2018-02-02'));
    sleep(1);
    $pets[] = new Cat('Базилио', new DateTime('2018-02-15'));

    // Немного черепах
    $pets[] = new Turtle('Рафаэль', new DateTime('2018-04-06'));
    sleep(1);
    $pets[] = new Turtle('Леонардо', new DateTime('2018-03-03'));
    sleep(1);
    $pets[] = new Turtle('Микеланджело', new DateTime('2018-06-04'));
    sleep(1);
    $pets[] = new Turtle('Донателло', new DateTime('2018-05-5'));

} catch (PetAgeException $e) {
    $e->printMessage();
    die();
}

// Помещаем животных в питомник, ловим исключение на неверный тим животного
try {
    foreach ($pets as $pet) {
        $shelter->getStorage()->push($pet);
    }
} catch (PetTypeException $e) {
    $e->printMessage();
    die();
}

// Выводим всё хранилище как есть
echo '<h3>Все животные:</h3>';
foreach ($shelter->getStorage()->getAll() as $pet) {
    echo 'Имя: ', $pet->getName(), '.<br />';
    echo 'Вид: ', end(explode('\\', get_class($pet))), '<br />';
    echo 'Дата рождения: ', $pet->getBirth()->format('Y-m-d'), '.<br />';
    echo 'Возраст: ', $pet->getAge()->y, ' лет, ', $pet->getAge()->m, ' месяцев, ', $pet->getAge()->d, ' дней', '.<br />';
    echo 'В питомнике уже: ', $pet->getStay()->s, ' секунд', '.<br />';
    echo '<br />';
}

echo '<h3>Черепахи, отсортированные по алфавиту:</h3>';
foreach ($shelter->getStorage()->getAllByType(Turtle::class, 'name') as $pet) {
    echo 'Имя: ', $pet->getName(), '.<br />';
    echo 'Вид: ', end(explode('\\', get_class($pet))), '<br />';
    echo 'Дата рождения: ', $pet->getBirth()->format('Y-m-d'), '.<br />';
    echo 'Возраст: ', $pet->getAge()->y, ' лет, ', $pet->getAge()->m, ' месяцев, ', $pet->getAge()->d, ' дней', '.<br />';
    echo 'В питомнике уже: ', $pet->getStay()->s, ' секунд', '.<br />';
    echo '<br />';
}

// Честно говоря вот тут я не понял что должно происходить с животным после передачи, поэтому просто удаляю его
// Возможно стоило создать класс человека и связывать эти классы, я бы это переспросил в реальной задаче.

echo '<h3>Передадим человеку собаку</h3>';
$dog = $shelter->getStorage()->pull(Dog::class);
echo '<pre>';
print_r($dog);
echo '</pre>';

echo '<h3>Передадим человеку животное находящееся приюте наибольшее время</h3>';
echo '<p>Не забываем, что одну собаку мы уже отдали выше.</p>';
$pet = $shelter->getStorage()->pull();
echo '<pre>';
print_r($pet);
echo '</pre>';